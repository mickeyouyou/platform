<?php

/**
 * 入口文件
 */
use Yaf\Application;
use Yaf\Exception;
use Yaf\Registry;

define('APP_NAME',      'Platform');
define('APP_CONF',      'Config');
define('APP_PATH',      realpath(dirname(__FILE__) . '/../'));
define('APP_INI',       APP_PATH.'/'.APP_CONF.'/'.APP_NAME.'.ini');
define('APP_LIB',       APP_PATH.'/../Library/');


try {
    $app = new Application(APP_INI);
    $app->bootstrap()->run();
} catch (Exception $e) {
    $msg =  "未捕获的异常，详细信息：\r\n" . "Uri: "     . @$_SERVER['REQUEST_URI'] . "\r\n" .
        "File: "    . $e->getFile() . "\r\n" .
        "Line: "    . $e->getLine() . "\r\n" .
        "Code: "    . $e->getCode() . "\r\n" .
        "Message: " . $e->getMessage() . "\r\n" .
        "Trace: "   . "\r\n" . $e->getTraceAsString() . "\r\n" .
        "Query: "   . var_export(Application::app()->getDispatcher()->getRequest()->getQuery(), true) . "\r\n" .
        "POST: "    . var_export(Application::app()->getDispatcher()->getRequest()->getPost(), true) . "\r\n" .
        "PARAMS: "  . var_export(Application::app()->getDispatcher()->getRequest()->getParams(), true) . "\r\n";

    Registry::get('Mount')->get('Logger')
        ->setLogger('Exception')
        ->log('error', $msg);
}
