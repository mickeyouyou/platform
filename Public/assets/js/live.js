/**
 * @copyright Copyright (c) fengzbao@qq.com.
 * Date: 16/4/10 Time: 下午8:57
 *
 */

var urls = {
    'search'     : '/live/dosearch',
    'getpushurl' : '/live/getpushurl',
    'getliveurl' : '/live/getliveurl',
    'close'      : '/live/close',
};

var machine_status = {
    0 : "未启用",
    1 : "已启用",
};


var live_status = {
    0 : "未开始",
    1 : "直播中",
    2 : "已中断",
    3 : "已结束",
};

var app = angular.module('platform', []);
app.controller('live', function($scope, $http) {
    // 订单列表查询
    $scope.queryList = function(page) {
        $scope.query = $scope.query || {};

        $http({
            method: 'POST',
            url: urls['search'],
            data: {
                //order_id:$scope.query.order_id,
                //user_id:$scope.query.user_id,
                //start_time:$scope.query.start_time,
                //end_time:$scope.query.end_time,
                //pay_type:$scope.query.pay_type,
                //status:$scope.query.status,
                page:page || 1,
            },
        })
            .success(function(response) {
                $scope.activities = response.activities;
                $scope.allno      = response.allno;
                $scope.prepage    = response.prepage;
                $scope.nextpage   = response.nextpage;
                $scope.live_status = live_status;
            });
    };

    $scope.getPushUrl = function(activityId) {
        $scope.query = $scope.activity_id;

        $http({
            method: 'POST',
            url: urls['getpushurl'],
            data: {
                activity_id:activityId,
            },
        })
            .success(function(response) {
                $scope.liveNum    = response.liveNum;
                $scope.lives     = response.lives;
                $scope.machine_status = machine_status;
            });
    };

    $scope.getLiveUrl = function(activityId) {
        $scope.query = $scope.activity_id;

        $http({
            method: 'POST',
            url: urls['getliveurl'],
            data: {
                activity_id:activityId,
            },
        })
            .success(function(response) {
                $scope.playPageUrl    = response.playPageUrl;
            });
    };

    $scope.close = function(activityId) {
        $scope.query = $scope.activity_id;

        $http({
            method: 'POST',
            url: urls['close'],
            data: {
                activity_id:activityId,
            },
        })
            .success(function(response) {
                //$scope.playPageUrl    = response.playPageUrl;
            });
    };

    $scope.queryList(1);
});


// 日期组件初始化
$('#start_timepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii'
});
$('#end_timepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii'
});


$('#exampleModal').on('show.bs.modal', function (event) {
    var $scope = angular.element("body").scope();
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    var activity_id = button.data('activityid');
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    $scope.getPushUrl(activity_id);
    var modal = $(this)
    modal.find('.modal-title').text('开始直播:' + recipient)
    modal.find('.modal-body input').val(recipient)
});


$('#liveUrl').on('show.bs.modal', function (event) {
    var $scope = angular.element("body").scope();
    var button = $(event.relatedTarget)
    var activity_id = button.data('activityid');
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    $scope.getLiveUrl(activity_id);
});
