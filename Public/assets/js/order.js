/**
 * @copyright Copyright (c) fengzbao@qq.com.
 * Date: 16/4/10 Time: 下午8:57
 *
 */

var urls = {
    'search': '/order/dosearch',
};



var app = angular.module('platform', []);
//        $httpProvider.defaults.transformRequest = function(data) {
//            if (data === undefined) {
//                return data;
//            }
//            return $.param(data);
//        };
//    }]);
app.controller('order', function($scope, $http) {
    // 订单列表查询
    // headers: {"Content-Type": "application/x-www-form-urlencoded"},
    $scope.queryList = function(page) {
        $scope.query = $scope.query || {};

        $http({
            method: 'POST',
            url: urls['search'],
            data: {
                order_id:$scope.query.order_id,
                user_id:$scope.query.user_id,
                start_time:$scope.query.start_time,
                end_time:$scope.query.end_time,
                pay_type:$scope.query.pay_type,
                status:$scope.query.status,
                page:page || 1,
            },
        })
            .success(function(response) {
                $scope.orders    = response.orders;
                $scope.allno     = response.allno;
                $scope.prepage   = response.prepage;
                $scope.nextpage  = response.nextpage;
            });
    };

    $scope.queryList(1);
});


// 日期组件初始化
$('#start_timepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii'
});
$('#end_timepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii'
});
