<?php
namespace Model;
use Core\Common;
use Core\Factory;
use Core\functions;
use General\Db\Sql\Select;

/**
 * Class CityModel
 */

class CityModel
{
    private $table = 'core_city';

    private $handler;

    public function __construct()
    {
        $this->handler = Factory::table(\Core\Common::dbTableNames($this->table));
    }

    /**
     * 增加业务城市
     * @param $data
     * @return mixed
     */
    public function add($data)
    {
        return $this->handler->create($data)->save();
    }

    public function count()
    {
        return count($this->handler->select());
    }

    public function get($where)
    {
        return $this->handler->get($where, true);
    }

    /**
     * 获取
     * @param $where
     * @return array
     */
    public function getCities($where)
    {
        return $this->handler->select();
    }

    public function delete($where)
    {
        return $this->handler->delete($where);
    }
}