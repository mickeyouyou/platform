<?php

/**
 * PHP SDK for  OpenAPI V1
 * @author fengzbao@qq.com
 * @copyright Copyright (c) fzb.me
 * @version $Id:1.0.0, LetvCloud.php, 2016-04-10 21:11 created (updated)$
 */

namespace Model;

/**
 * 提供访问乐视云视频开放平台的接口
 */
class LetvCloudLiveModel
{

    public $user_unique   = '2eb66ef190';
    public $user_id       = '152367';
    public $secret_key    = '051cb5c9837158f073303f7783a73adb';
    public $restUrl       = 'http://api.open.letvcloud.com/live/execute';
    public $format        = 'json';
    protected $apiVersion = '3.0';

    /**
     * @param $activityParams
     * @return string
     */
    public function createActivity ($activityParams)
    {
        $method = 'letv.cloudlive.activity.create';
        return self::httpPost($method, $activityParams);
    }

    /**
     * @param $activityParams
     * @return string
     */
    public function modify($activityParams)
    {
        $method = 'letv.cloudlive.activity.modify';
        return self::httpPost($method, $activityParams);
    }

    /**
     *
     * @param null $activity
     * @return string || Activity List
     */
    public function activityQuery($activity = null)
    {
        $method = 'letv.cloudlive.activity.search';
        $param['activityId'] = $activity['activityId'];
        $param['activityName'] = $activity['activityName'];
        $param['activityStatus'] = $activity['activityStatus'];
        return self::httpGet($method);
    }

    /**
     * 结束活动接口（ver=3.0）
     * @param $activityId
     * @return string
     */
    public function closeActivity($activityId)
    {
        $method= 'letv.cloudlive.activity.stop';
        $param['activityId'] = $activityId;
        return self::httpPost($method, $param);
    }

    /**
     * 直播活动封面上传（ver=3.0）支持格式jpg、png、gif
     * @param $activityId
     * @param File $file
     * @return string
     */
    public function modifyCoverImg($activityId, $file)
    {
        $method = 'letv.cloudlive.activity.modifyCoverImg';

        $params['activityId'] = $activityId;
        $params['file'] = $file;
        return self::httpPost($method, $params);
    }

    /**
     * Get
     * @param $activityId
     * @return string
     */
    public function getliveUrl($activityId)
    {
        $method = 'letv.cloudlive.activity.playerpage.getUrl';
        $params['activityId']  = $activityId;
        return self::httpGet($method, $params);
    }

    /**
     * get push Url
     * @param $activityId
     * @return string
     */
    public function getPushUrl($activityId)
    {
        $method = 'letv.cloudlive.activity.getPushUrl';
        $params['activityId'] = $activityId;
        return self::httpGet($method, $params);
    }

    /**
     * 直播CDN打点录制任务创建接口（ver=3.0）
     * @param $streamName
     * @param $appName
     * @param $domain
     * @param $fileType
     * @param $startTime
     * @param $endTime
     * @return string
     */
    public function createCdnRecTaskByStreamName($streamName, $appName, $domain, $fileType, $startTime, $endTime)
    {
        $method = 'lecloud.cdnlive.rec.createCdnRecTaskByStreamName';

        $params['streamName'] = $streamName;
        $params['appName']    = $appName;
        $params['domain']     = $domain;
        $params['fileType']   = $fileType;
        $params['startTime']  = $startTime;
        $params['endTime']    = $endTime;

        return self::httpPost($method, $params);
    }

    /**
     * 打点录制结果查询接口（ver=3.1）
     * @param $liveId
     * @param $taskId
     * @param $offset
     * @param $size
     * @param $startTime
     * @param $endTime
     * @return string
     */
    public function searchResult($liveId, $taskId, $offset, $size, $startTime, $endTime)
    {
        $method = 'lecloud.cloudlive.rec.searchResult';

        $params = array(
            'liveId'    => $liveId,
            'taskId'    => $taskId,
            'offset'    => $offset,
            'size'      => $size,
            'startTime' => $startTime,
            'endTime'   => $endTime,
        );

        return self::httpGet($method, $params);
    }

    public function getDownloadUrl($taskId)
    {
        $method = 'lecloud.cdnlive.rec.downloadUrl.get';

        $params['taskId'] = $taskId;
        return self::httpGet($method, $params);

    }

    /**
     * 活动安全设置接口（ver=3.0）
     * @param $activityId
     * @param $neededPushAuth
     * @param $pushUrlValidTime
     * @param $liveKey
     * @param $needIpWhiteList
     * @param $pushIpWhiteList
     * @param $needPlayerDomainWhiteList
     * @param $playerDomainWhiteList
     */
    public function sercurity($activityId, $neededPushAuth, $pushUrlValidTime, $liveKey, $needIpWhiteList, $pushIpWhiteList, $needPlayerDomainWhiteList, $playerDomainWhiteList)
    {
        $method = 'letv.cloudlive.activity.sercurity.config';

        $params['activityId'] = $activityId;
        $params['neededPushAuth'] = $neededPushAuth; // 是否启用推流鉴权 0：否 1：是
        $params['pushUrlValidTime'] = $pushUrlValidTime; // 推流地址有效时长，单位s，启用推流鉴权时有效
        $params['liveKey'] = $liveKey; // 直播安全码，计算推流地址时用到的安全码，如果为空的话，则使用客户的安全码
        $params['needIpWhiteList'] = $needIpWhiteList;// 是否启用IP推流白名单 0：否 1：是。
        $params['pushIpWhiteList'] = $pushIpWhiteList;
        $params['needPlayerDomainWhiteList'] = $needPlayerDomainWhiteList;
        $params['playerDomainWhiteList'] = $playerDomainWhiteList;// 域名白名单，多个时逗号分隔。最大长度为512，最多为10个

        self::httpPost($method, $params);
    }

    /**
     * 获取推流token接口（ver=3.0）
     * 用于调用乐视云上传SDK使用。
     * @param $activityId
     * @return string
     */
    public function getPushToken($activityId)
    {
        $method = 'letv.cloudlive.activity.getPushToken';

        $params['activityId'] = $activityId;
        return self::httpGet($method, $params);
    }

    /**
     * 获取录制视频信息接口（ver=3.0
     * 查询录制视频的videoId和videoUnique，其中videoUnique组合成播放地址。
     * @param $activityId
     * @return string
     */
    public function getPlayInfo($activityId)
    {
        $method = 'letv.cloudlive.activity.getPlayInfo';
        $params['activityId'] = $activityId;

        return self::httpGet($method, $params);
    }

    /**
     * 构造云视频Sign
     * @param array $params 业务参数
     * @return string
     */
    public function generateSign ($params)
    {
        ksort ($params);
        $keyStr = '';
        foreach ($params as $key => $value) {
            $keyStr .= $key . $value;
        }
        $keyStr .= $this->secret_key;
        $key = md5($keyStr);

        return $key;
    }

    /**
     * 发送http请求
     * @param $url 请求地址
     * @param $postFields HTTP方法为POST时的请求参数
     * @return string HTTP请求相应结果
     * @throws \Exception
     * @throws \Model\Exception
     */
    public function curl ($url, $postFields = null)
    {
        $ch = curl_init ();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_FAILONERROR, false);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        //https 请求
        if (strlen ($url) > 5 && strtolower (substr ($url, 0, 5)) == "https") {
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        if (is_array ($postFields) && 0 < count ($postFields)) {
            $postBodyString = "";
            $postMultipart = false;
            foreach ($postFields as $k => $v) {
                if ("@" != substr ($v, 0, 1))//判断是不是文件上传
                {
                    $postBodyString .= "$k=" . urlencode ($v) . "&";
                } else//文件上传用multipart/form-data，否则用www-form-urlencoded
                {
                    $postMultipart = true;
                }
            }
            unset($k, $v);
            curl_setopt ($ch, CURLOPT_POST, true);
            if ($postMultipart) {
                curl_setopt ($ch, CURLOPT_POSTFIELDS, $postFields);
            } else {
                curl_setopt ($ch, CURLOPT_POSTFIELDS, substr ($postBodyString, 0, -1));
            }
        }
        $reponse = curl_exec ($ch);

        if (curl_errno ($ch)) {
            throw new \Exception(curl_error ($ch), 0);
        } else {
            $httpStatusCode = curl_getinfo ($ch, CURLINFO_HTTP_CODE);
            if (200 !== $httpStatusCode) {
                throw new \Exception($reponse, $httpStatusCode);
            }
        }
        curl_close ($ch);

        return $reponse;
    }


    public function httpGet($method, $apiParams)
    {
        //组装系统参数
        $sysParams['userid']    = $this->user_id;
        $sysParams['timestamp'] = time()*1000;
        $sysParams['ver']       = $this->apiVersion;
        $sysParams["method"]    = $method;
        //参数集合=系统参数+业务参数
        $params = array_merge ($sysParams, $apiParams);
        //构造请求URL
        $resurl = '';
        $resurl .= $this->restUrl;
        //签名
        $params['sign'] = $this->generateSign($params);
        //参数放入GET请求串
        foreach ($params as $key => $v) {
            if (!strpos ($resurl, '?')) {
                $resurl .= "?{$key}=" . urlencode ($v);
            } else {
                $resurl .= "&{$key}=" . urlencode ($v);
            }
        }
        //发起HTTP请求
        $respObj = $this->curl($resurl);

        return $respObj;

    }

    /**
     * 获取HTTP请求结果
     * @param $api API名称
     * @param $apiParams API业务参数
     * @return string
     */
    public function httpPost($api, $apiParams)
    {
        // 组装系统参数
        $sysParams['userid'] = $this->user_id;
        $sysParams['timestamp'] = time() * 1000;
        $sysParams['ver'] = $this->apiVersion;
        $sysParams['method'] = $api;
        // 参数集合=系统参数+业务参数
        $params = array_merge($sysParams, $apiParams);
        //构造请求URL
        $resurl = '';
        $resurl .= $this->restUrl;
        //签名
        $params['sign'] = $this->generateSign($params);
        //参数放入GET请求串
        foreach ($params as $key => $v) {
            if (!strpos ($resurl, '?')) {
                $resurl .= "?{$key}=" . urlencode ($v);
            } else {
                $resurl .= "&{$key}=" . urlencode ($v);
            }
        }
        //发起HTTP请求
        $respObj = $this->curl($this->restUrl, $params);

        return $respObj;
    }

}