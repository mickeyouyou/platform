<?php
/**
 * @author fengzbao@qq.com
 * @copyright Copyright (c) fzb.me
 * @version $Id:1.0.0, Message.php, 2016-03-27 00:31 created (updated)$
 */

namespace Model;

use Core\Common;
use Core\Factory;
use General\Db\Sql\Select;
use General\Db\Table\Pagination;

class MessageModel
{

    private $handler;


    public function __construct ()
    {
        $table = Common::dbTableNames('core_message');
        $this->handler = Factory::table($table);
    }


    public function add($data)
    {
        return $this->handler->create($data)->save();
    }


    public function getOne(array $where)
    {
        return $this->handler->get($where, true);
    }

    public function updateMessage($where, $data)
    {
        return $this->handler->update($data, $where);
    }


    public function getList($where, $page)
    {
        $pagination = new Pagination($page, 10);

        return $this->handler->select(function(Select $select) use($where) {
            $select->columns('*');
            $select->where($where);
            $select->order('id desc');
        }, $pagination);
    }


    public function delete($id)
    {
        return $this->handler->delete(array('id' => $id));
    }

    public function count($where = null)
    {
        return count($this->handler->select($where));
    }

}