<?php

namespace Model;

use Core\Factory;

class ChapterModel
{
    private $table = 'core_chapter';

    private $handler;

    /**
     *
     */
    public function __construct()
    {
        $this->handler = Factory::table(\Core\Common::dbTableNames($this->table));
    }

    /**
     * @param $data
     * @return int
     */
    public function add($data)
    {

        return $this->handler->create($data)->save();

    }

    public function get($where)
    {
        return $this->handler->select($where);
    }



    public function count()
    {
        return count($this->handler->select());
    }

    /**
     * delete chapter
     * @param $cid
     * @return int
     */
    public function delete($cid)
    {
        return $this->handler->delete(array('course_id' => $cid));
    }
}
