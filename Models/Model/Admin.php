<?php
/**
 * @author fengzbao@qq.com
 * @copyright Copyright (c) Beijing LuboTianDi Technology Co.,Ltd.
 * @version $Id:1.0.0, User.php, 2015-08-01 15:03 created (updated)$
 */

namespace Model;

use Core\Factory;

class AdminModel
{
    private $table = 'core_admin_users';

    private $handler;

    /**
     *
     */
    public function __construct()
    {
        $this->handler = Factory::table(\Core\Common::dbTableNames($this->table));
    }

    /**
     * @param $data
     * @return int
     */
    public function add($data)
    {
        return $this->handler->create($data)->save();
    }

    public function get($where)
    {
        return $this->handler->select($where);
    }

}