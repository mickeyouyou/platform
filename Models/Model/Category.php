<?php

namespace Model;

use Core\Factory;

class CategoryModel
{
    private $table = 'core_category';

    private $handler;

    /**
     *
     */
    public function __construct()
    {
        $this->handler = Factory::table(\Core\Common::dbTableNames($this->table));
    }

    /**
     * @param $data
     * @return int
     */
    public function add($data)
    {
        $data['created_time'] = date('Y-m-d H:i:s');
        return $this->handler->create($data)->save();
    }

    /**
     * @param $where
     * @return array
     */
    public function get($where)
    {
        return $this->handler->select($where);
    }

    /**
     * @param $map
     * @return array|\General\Db\Row\AbstractRow|null
     */
    public function getOne($map)
    {
        return $this->handler->get($map, true);

    }

    /**
     * @param $data
     * @param $map
     * @return int
     */
    public function update($data, $map)
    {
        $data['updated_time'] = date('Y-m-d H:i:s');
        return $this->handler->update($data, $map);
    }


    public function delete($map)
    {
        return $this->handler->delete($map);
    }
}