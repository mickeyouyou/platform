<?php
namespace Model;
use Core\Factory;

class BannerModel
{
    private $table = 'core_banner';

    private $handler;

    /**
     *
     */
    public function __construct()
    {
        $this->handler = Factory::table(\Core\Common::dbTableNames($this->table));
    }

    /**
     * @param $data
     * @return int
     */
    public function add($data)
    {

        return $this->handler->create($data)->save();
    }

    public function get($where)
    {
        return $this->handler->select($where);
    }

    /**
     * update one
     * @param $set
     * @param $where
     * @return int
     */
    public function update($set, $where)
    {
        return $this->handler->update($set, $where);

    }


    /**
     * delete
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->handler->delete(array('id' => $id));
    }
}