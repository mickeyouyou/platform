<?php
namespace Model;
use Core\Factory;

class ClassModel
{
    private $table = 'core_class';

    private $handler;

    /**
     *
     */
    public function __construct()
    {
        $this->handler = Factory::table(\Core\Common::dbTableNames($this->table));
    }

    /**
     * @param $data
     * @return int
     */
    public function add($data)
    {

       return $this->handler->create($data)->save();
    }

    public function get($where)
    {
        return $this->handler->select($where);
    }

    /**
     * delete class
     * @param $cid
     * @return int
     */
    public function delete($cid)
    {
        return $this->handler->delete(array('course_id' => $cid));
    }
}