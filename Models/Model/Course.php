<?php
namespace Model;
use Core\Factory;

class CourseModel
{
    private $table = 'core_course';

    private $handler;

    /**
     *
     */
    public function __construct()
    {
        $this->handler = Factory::table(\Core\Common::dbTableNames($this->table));
    }

    /**
     * @param $data
     * @return int
     */
    public function add($data)
    {

        return $this->handler->create($data)->save();
    }

    public function get($where)
    {
        return $this->handler->select($where);
    }

    public function getOne($where)
    {
        return $this->handler->get($where, true);
    }


    public function count()
    {
        return count($this->handler->select());
    }

    /**
     * update one course
     * @param $set
     * @param $where
     * @return int
     */
    public function update($set, $where)
    {
        return $this->handler->update($set, $where);

    }


    /**
     * delete course
     * @param $cid
     * @return int
     */
    public function delete($cid)
    {
        return $this->handler->delete(array('id' => $cid));
    }
}