<?php
/**
 * @author fengzbao@qq.com
 * @copyright Copyright (c) fzb.me
 * @version $Id:1.0.0, User.php, 2015-08-01 15:03 created (updated)$
 */

namespace Model;

use Core\Factory;
use General\Db\Sql\Select;
use General\Db\Table\Pagination;

class UserModel
{
    private $table = 'core_users';

    private $handler;

    /**
     *
     */
    public function __construct()
    {
        $this->handler = Factory::table(\Core\Common::dbTableNames($this->table));
    }

    /**
     * @param $data
     */
    public function add($data)
    {
        $this->handler->create($data)->save();

    }

    public function count($where = null)
    {
        return count($this->handler->select($where));
    }

    public function get($where, $pageNo)
    {
        $pagination = new Pagination($pageNo, ORDER_PAGE_SIZE);

        return $this->handler->select(function(Select $select) use($where) {
            $select->columns('*');
            $select->where($where);
            $select->order('uid desc');
        }, $pagination);
    }

    public function getOne($where)
    {
        return $this->handler->get($where, true);
    }

    /**
     * @param $where
     * @return array|\General\Db\Row\AbstractRow|null
     */
    public function search($where)
    {
        return $this->handler->get($where, true);

    }

}