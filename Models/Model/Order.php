<?php

namespace Model;

use Core\Factory;
use General\Db\Sql\Select;
use General\Db\Table\Pagination;

class OrderModel
{
    private $table = 'core_order';

    private $handler;

    /**
     *
     */
    public function __construct()
    {
        $this->handler = Factory::table(\Core\Common::dbTableNames($this->table));
    }

    /**
     * @param $data
     * @return int
     */
    public function add($data)
    {
        return $this->handler->create($data)->save();

    }

    public function count($where = null)
    {
        return count($this->handler->select($where));
    }

    /**
     * 订单分页模型方法
     * @param $where
     * @param $pageNo
     * @return array
     * @internal param $offset
     */
    public function get($where, $pageNo)
    {
        $pagination = new Pagination($pageNo, ORDER_PAGE_SIZE);

        $list = $this->handler->select(function(Select $select) use($where) {
            $select->columns('*');
            $select->where($where);
            $select->order('id desc');
//            \Yaf\Registry::get('Mount')->get('Logger')->log('debug',  $select->getSqlString());
        }, $pagination);


        return $list;
    }


    public function update($data, $where)
    {
        return $this->handler->update($data, $where);

    }

    /**
     * 关闭订单
     * @param $order_id
     * @return int
     */
    public function close($order_id)
    {
        return $this->handler->update(array(
            'status' => ORDER_CLOSE_CODE
        ), array(
            'order_id' => $order_id
        ));
    }
}
