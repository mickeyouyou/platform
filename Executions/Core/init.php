<?php

ini_set('seaslog.use_buffer', 'Off');//关闭sealog的缓存存储


define('APP_NAME',      'Platform');
define('APP_CONF',      'Config');
define('APP_PATH',      realpath(dirname(__DIR__).'/../'));
define('APP_INI',       APP_PATH . '/' . APP_CONF . '/' . APP_NAME . '.ini');
define('APP_LIB',       APP_PATH . '/../Library/');
define('APP_START',     microtime(true));
define('EXE_PATH',      APP_PATH . '/' . 'Executions');

use Yaf\Application;

$app = new Application(APP_INI);
$app->bootstrap();
