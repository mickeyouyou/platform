<?php
/**
 * @author fengzbao@qq.com
 * @copyright Copyright (c) fzb.me
 * @version $Id:1.0.0, live.php, 2016-04-11 00:03 created (updated)$
 */

use Model\LetvCloudLiveModel;

include dirname(__DIR__) . '/Core/' . 'init.php';


$activity = array(
    'activityName' => 'test',
    'startTime'    => '20160412000000',
    'endTime'      => '20160412000100',
//    'coverImgUrl'  => 'test.jpg',
    'description'  => 'test_description',
    'liveNum'       => 4, // 机位数量
    'codeRateTypes' => '10', // 取值范围：10 流畅；13 标清；16 高清；19 超清；22 720P；25 1080P；99 原画
    'needRecord'    => 1,
    'needTimeShift' => 1,
    'needFullView'  => 1,
    'activityCategory' => '012', // education
    'playMode'         => 0, // 实时直播,1延时直播
);

$letvapi = new LetvCloudLiveModel();

$result = $letvapi->createActivity($activity);
var_dump($result);