<?php
use Yaf\Application;
use Yaf\Bootstrap_Abstract;
use Yaf\Dispatcher;
use Yaf\Registry;
use Yaf\Route\Regex;

/**
 * 
 *
 * @author fengzbao@qq.com 
 * @copyright Copyright (c) fzb.me
 * @version $Id:1.0.0, Bootstrap.php, 2015-05-11 15:27 created (updated)$
 */


class Bootstrap extends Bootstrap_Abstract
{

    /**
     * 应用初始化
     *
     * @param Dispatcher $dispatcher
     * @return void
     */
    public function _initApplication(Dispatcher $dispatcher)
    {
        // 导入全局常量
        $defines = Application::app()->getConfig()->application->defines->toArray();
        if (isset($defines['static_version'])) {
            $release = $defines['static_version'];
            unset($defines['static_version']);
        }
        foreach ($defines as $index => $value) {
            define(strtoupper($index), $value);
        }

        if (isset($release) &&  ini_get('yaf.environ') == 'online') {
            // 静态资源文件版本常量
            define('STATIC_VERSION', $release);
        } else {
            // 静态资源文件版本常量
            define('STATIC_VERSION', '?' . microtime(true));
        }

        // 注册Mount对象
        $Mount = General\Mount\Mount::getInstance();
        Registry::set("Mount", $Mount);

    }

    /**
     * 初始化配置加载器
     *
     * @param Dispatcher $dispatcher
     * @return void
     */
    public function _initConfigLoader(Dispatcher $dispatcher) {
        $config = function($type, $ini = true) {
            if ($ini) {
                $ext = '.ini';
            } else {
                $ext = '.php';
            }
            $configFile = APP_PATH . '/' . APP_CONF . '/' . ucfirst($type) . $ext;

            if (!file_exists($configFile)) {
                return new Yaf\Config\Simple(array());
            }

            if ($ini) {
                $config = new Yaf\Config\Ini($configFile);
                $configSection = $config->get(ini_get('yaf.environ'));
                if (count($configSection) > 0) {
                    unset($config);
                    $config = $configSection;
                }
            } else {
                $configAll = include $configFile;

                if (!isset($configAll[ini_get('yaf.environ')])) {
                    $configArray = $configAll;
                } else {
                    $configArray = $configAll[ini_get('yaf.environ')];
                }

                foreach($configArray as $name => $option) {
                    if(!is_string($option) && !is_callable($option) && !is_object($option)) {
                        $configConstant[$name] = $option;
                    } else {
                        if (is_string($option)) {
                            $configConstant[$name] = $option;
                        } else {
                            Registry::get('Mount')->mount($name, $option);
                        }
                    }
                }

                $config = new Yaf\Config\Simple($configConstant, true);
            }

            return $config;
        };

        Registry::get('Mount')->mount('ConfigLoader', $config);


        // 7. 订单状态常量定义
        $defines = Registry::get('Mount')->get('ConfigLoader', 'Defines');

        foreach($defines->defines as $define => $code)
        {
            define($define, $code);
        }

        $userDefines = get_defined_constants(true)['user'];
        foreach($defines->descs as $name => $descs)
        {
            $staticDescs = array();
            foreach($descs as $key => $desc)
            {
                $staticDescs[$userDefines[$key]] = $desc;
            }
            Registry::set($name, $staticDescs);
        }
    }

    /**
     * 日志初始化
     *
     * @param Dispatcher $dispatcher
     * @return void
     */
    /**
     * 日志初始化
     *
     * @param Dispatcher $dispatcher
     */
    public function _initLogger(Dispatcher $dispatcher) {
        $logger = function() use($dispatcher) {
            $logConfig = Yaf\Registry::get('Mount')->get('ConfigLoader', 'log');

            if ($logConfig instanceof Yaf\Config\Ini) {
                $basePath   = isset($logConfig['base_path']) ? $logConfig['base_path'] : '';
                $module     = isset($logConfig['module']) ? $logConfig['module'] : '';
                $loggerName = isset($logConfig['logger']) ? $logConfig['logger']: 'NullLogger';
            } else {
                $loggerName = 'NullLogger';
            }
            $loggerClassName = '\\General\\Psr\\Log\\' . $loggerName;

            if ($loggerName != 'NullLogger' && class_exists($loggerClassName, true)) {
                $logger = new $loggerClassName();
                if (strtolower($loggerName) == 'seaslogger') {
                    if ($basePath) {
                        $logger->setBasePath($basePath);
                    }

                    if ($module) {
                        $logger->setLogger($module);
                    }

                    $module = $dispatcher->getRequest()->getModuleName();
                    if ($module) {
                        $logger->setLogger($module);
                    }
                }
            } else {
                $logger = new $loggerClassName();
            }

            return $logger;
        };

        Registry::get('Mount')->mount('Logger', $logger);
    }

    /**
     * 支持数据库分库分表定位数据库实例
     *
     * @param Dispatcher $dispatcher
     * @return void
     */
    public function _initDbTableMap(Dispatcher $dispatcher) {
        $mapper = function () {
            $mapperTableDBServer = array();
            $database = Registry::get('Mount')->get('ConfigLoader', 'database');
            if (empty($database->servers)) {
                throw new Exception('Mount Mapper Error. No DB Server List');
            }

            $servers  = explode(',', $database->servers);
            foreach ($servers as $server) {
                if (isset($database->{$server}['tables'])) {
                    foreach ($database->{$server}->tables as $table) {
                        $mapperTableDBServer[$table] = $server;
                    }
                }
            }

            return $mapperTableDBServer;
        };

        Yaf\Registry::get('Mount')->mount('Mapper', $mapper);
    }

}
