<?php
use Core\BaseController;
use Model\CategoryModel;
use Model\CityModel;
use Model\LetvCloudLiveModel;
use Model\LiveModel;
use Yaf\Dispatcher;

/**
 * live controller based on letvcloud.com
 * @author fengzbao@qq.com
 * @copyright Copyright (c) fzb.me
 * @version $Id:1.0.0, Live.php, 2016-04-10 21:11 created (updated)$
 */
class LiveController extends BaseController
{

    public $cityModel;
    public $categoryModel;
    public $letvApi;
    public $liveModel;

    public function init ()
    {
        $this->cityModel = new CityModel();
        $this->categoryModel = new CategoryModel();
        $this->letvApi= new LetvCloudLiveModel();
        $this->liveModel = new LiveModel();
    }


    public function indexAction()
    {


    }


    public function addAction()
    {
        $city_id = $this->getRequest()->getQuery('city_id', 0);
        $city = $this->cityModel->get(array(
            'city_id' => $city_id
        ));

        // read cate_ids by city_id
        $categories = $this->categoryModel->get(array(
            'city_id' => $city_id
        ));
        $this->assign('city_id', $this->getRequest()->getQuery('city_id'));
        $this->assign('city', $city);
        $this->assign('categories', $categories);

    }


    public function doAddAction()
    {
        \Yaf\Dispatcher::getInstance()->disableView();
        $image = \Core\Common::uploadAttachmentReturnKeys();

        // 1.部分字段检查
        $fields = array(
            'name',
            'start_time',
            'end_time',
            'description',
        );

        foreach($fields as $i => $field)
        {
            if(empty($field)) {
                $response = array(
                    'errorNo'  => $i,
                    'errorMsg' => $field . "must be not empty~",
                );
                $this->sendHttpOutput($response);
                return;
            }
        }
        // 数据整理
        $data = array (
            'name'          => $this->getRequest()->getPost('name'),
            'start_time'    => $this->getRequest()->getPost('start_time'),
            'end_time'      => $this->getRequest()->getPost('end_time'),
            'description'   => $this->getRequest()->getPost('description'),
            'is_record'     => $this->getRequest()->getPost('is_record'),
            'is_timeshift'  => $this->getRequest()->getPost('is_timeshift'),
            'is_fullview'   => $this->getRequest()->getPost('is_fullview'),
            'play_mode'     => $this->getRequest()->getPost('play_mode'),
            'coderate_type' => implode(',', $this->getRequest()->getPost('coderate_type')),
            'live_num'      => $this->getRequest()->getPost('live_num'),
            'city_id'       => $this->getRequest()->getPost('city_id'),
            'cover_img'     => $image['cover_img'],
            'create_time'   => date('Y-m-d H:i:s'),
        );

        // $data['status'] = (time() < strtotime($data['start_time']))? 0 : 1;

        // 2.请求API
        $activity = array(
            'activityName'     => $data['name'],
            'startTime'        => date('YmdHis', strtotime($data['start_time'])),
            'endTime'          => date('YmdHis', strtotime($data['end_time'])),
            'coverImgUrl'      => APP_CDN . $image['cover_img'],
            'description'      => $data['description'],
            'liveNum'          => $data['live_num'], // 机位数量
            'codeRateTypes'    => $data['coderate_type'], // 10 流畅；13 标清；16 高清；19 超清；22 720P；25 1080P；99 原画
            'needRecord'       => $data['is_record'],
            'needTimeShift'    => $data['is_timeshift'],
            'needFullView'     => $data['is_fullview'],
            'activityCategory' => '012', // education
            'playMode'         => $data['play_mode'], // 实时直播,1延时直播
        );

        $json = $this->letvApi->createActivity($activity);
        $activityId = json_decode($json, true);

        // 3.增加到数据库
        $id = $this->liveModel->add($data);

        // 4.更新数据库活动ID
        $result = $this->liveModel->update(array(
            'activity_id' => $activityId['activityId'],
            'update_time' => date('Y-m-d H:i:s'),
        ), array(
            'id' => $id,
        ));

        if($result) {
            $this->redirect('./index');
        }
    }


    public function dosearchAction()
    {
        Dispatcher::getInstance()->disableView();
        $postData = json_decode(file_get_contents('php://input'), true);

        $pageNo = $postData['page'];

        $where = array();
        if(isset($postData['name'])) {
            $where[] = 'name like "% ' . $postData['name'] . '"';
        }

        if(isset($postData['activity_id'])) {
            $where[] = 'activity_id = ' . $postData['activity_id'];
        }

        if(isset($postData['start_time'])) {
            $where[] =  "pay_time >= '" . $postData['start_time'] . "'";
        }
        if(isset($postData['end_time'])) {
            $where[] = "pay_time < '" . $postData['end_time'] . "'";
        }

        if(isset($postData['start_time'])) {
            $where[] = 'status = ' . $postData['status'];
        }

        $allno = $this->liveModel->count($where);
        $lives = $this->liveModel->get($where, $pageNo);

        $displayArray = array(
            'activities' => $lives,
            'allno'      => $allno,
            'prepage'    => $pageNo - 1 == 0 ? 1 : $pageNo-1,
            'nextpage'   => $pageNo + 1
        );

        $this->sendHttpOutput($displayArray);
    }

    /**
     *
     */
    public function getPushUrlAction()
    {
        \Yaf\Dispatcher::getInstance()->disableView();
        $postData = json_decode(file_get_contents('php://input'), true);
        $activityId = $postData['activity_id'];

        $responseJson = $this->letvApi->getPushUrl($activityId);
        $respose = json_decode($responseJson, true);

        $this->sendHttpOutput($respose);
    }

    /**
     *
     */
    public function getLiveUrlAction()
    {
        \Yaf\Dispatcher::getInstance()->disableView();
        $postData = json_decode(file_get_contents('php://input'), true);
        $activityId = $postData['activity_id'];

        $responseJson = $this->letvApi->getliveUrl($activityId);
        $respose = json_decode($responseJson, true);

        $this->log('debug', var_export($respose, true));

        $this->sendHttpOutput($respose);

    }


    public function editAction()
    {
        \Yaf\Dispatcher::getInstance()->disableView();
        var_dump($_SERVER);
    }

    /**
     * @throws \Exception
     */
    public function closeAction()
    {
        Dispatcher::getInstance()->disableView();
        $activityId = $this->getRequest()->getQuery('activity_id');

        if(empty($order_id)) {
            throw new Exception('直播活动ID为空');
        }

        // 1.请求LETVCLOUD接口
        $this->letvApi->closeActivity($activityId);

        // 2.更新数据库
        $updated = $this->liveModel->update(array(
            'is_delete' => 1,
        ), array(
            'activity_id' => $activityId,
        ));

        if($updated) {
            $this->redirect('./index');
        }
    }

}