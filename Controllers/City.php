<?php
/**
 * 业务城市管理
 *
 * @author fengzbao@qq.com
 * @copyright Copyright (c) Beijing Jinritemai Technology Co.,Ltd.
 * @version $Id:1.0.0, City.php, 2015-06-23 23:29 created (updated)$
 */

use Core\BaseController;
use Core\Basic;
use Model\CityModel;
use Yaf\Dispatcher;

class CityController extends BaseController
{

    /**
     *
     *
     * @var object 城市模型
     */
    private $cityModel;

    public function init()
    {
        parent::init();
        $this->cityModel = new CityModel();
    }

    /**
     * 业务城市列表
     */
    public function indexAction()
    {
        $cities = $this->cityModel->getCities(array());

        $this->getView()->assign('cities', $cities);
    }

    /**
     * 新增业务城市
     */
    public function addAction()
    {
        $city = $this->getRequest()->getPost('city', '');

        if(empty($city)) {

            $this->redirect('./index');
        }

        $this->cityModel->add(array(
            'city_name' => $city,
            'create_time' => date('Y-m-d H:i:s')
        ));
        $this->redirect('./index');
    }

    /**
     * delete business city
     */
    public function deleteAction()
    {
        //todo 移除城市，移除所有相关的业务
        Dispatcher::getInstance()->disableView();
        $cityId = $this->getRequest()->getQuery('city_id');
        $this->cityModel->delete(array('city_id' => $cityId));
        $this->redirect('./index');

    }

}