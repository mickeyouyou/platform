<?php
use Core\BaseController;
use Core\Basic;
use Core\Factory;
use Core\Common;

class ImageController extends BaseController
{
    public $thumbConf;
    public $qiniu;
    public $imgDomain;
    public function init()
    {
        parent::init();
        //读取缩略图配置文件
        $this->thumbConf = Yaf\Registry::get('Mount')->get('ConfigLoader', 'thumb');
        $this->qiniu = \Core\Factory::file('qiniu');
        $domain = rtrim($this->qiniu->getDomain(), '/') . '/';
        if (strpos($domain, 'http') === false) {
            $domain = 'http://' . $domain;
        }
        $this->imgDomain = $domain;
        Yaf\Dispatcher::getInstance()->disableView();
    }

    /**
     * 单文件上传
     * 
     * @access public
     * @return json
     */
    public function singleUploadAction()
    {
        $response = array(
            'location' => '',
            'url' => '',
            'thumb_url' => '',
        );
        $keys = Common::uploadAttachmentReturnKeys();
        if(isset($keys['file']))
        {
            $response['location'] = $keys['file'];
            $response['url'] = Common::getResourceUrl($keys['file']);
        }

        $thumb_width = $this->getRequest()->getQuery('thumb_width');
        $thumb_height = $this->getRequest()->getQuery('thumb_height');
        if($thumb_width && $thumb_height)
        {
            $qiniu = Factory::file('qiniu');
            $response['thumb_url'] = $qiniu->imageThumb($response['location'], '0', $thumb_width, $thumb_height);
        }

        $this->sendHttpOutput($response);
    }

    /**
     * 文件上传
     *
     * @access public
     * @return json
     */
    public function fileUploadAction()
    {
        //获取图片类型
        $type = $this->getRequest()->getQuery('type');

        //读取图片配置
        $typeWidth = $type."_width";
        $typeHeight = $type."_height";
        $typeSize = $type."_size";
        $typeType = $type."_type";

        $width = $this->thumbConf->$typeWidth;
        $height = $this->thumbConf->$typeHeight;
        $imgSize = $this->thumbConf->$typeSize;
        $imgType = $this->thumbConf->$typeType;

        //如果要求是正方形则宽高可以放宽
        if($width == $height){
            $maxWidth = $this->thumbConf->product_max_width;
            $maxHeight = $this->thumbConf->product_max_height;
            $maxSize = $this->thumbConf->product_max_size;
        }

        if(empty($_FILES)){
            $response = array('error' => 1, 'msg' => "上传失败");
        }else{
            $files = @$this->getRequest()->getFiles()['newfile'];
            if (empty($files)) {
                $files = @$this->getRequest()->getFiles()['up_file'];
            }

            $fileParts = pathinfo($files['name']);

            //验证图片格式
            if($imgType == "png"){
                $fileTypes = array('png', 'PNG'); // File extensions
            }else{
                $fileTypes = array('jpg', 'jpeg', 'JPEG', 'JPG'); // File extensions
            }
            $keys = \Core\Common::uploadAttachmentReturnKeys();
            if (isset($keys['newfile'])) {
                $newFile = $keys['newfile'];
            }
            if (isset($keys['up_file'])) {
                $newFile = $keys['up_file'];
            }

            //实际地址
            $path = \Core\Common::getLocalAttachmentPathName($newFile);
            //原图放入云存储
            $originalImgName = "original_".$newFile;
            $originalExistsstatus = $this->qiniu->exists($originalImgName);
            if (!$originalExistsstatus) {
                $this->qiniu->write($originalImgName, $path);
            }
            $size = getimagesize($path);
            $filesize = filesize($path) / 1024;

            if(!in_array($fileParts['extension'], $fileTypes)){
                $response = array('error' => 2, 'msg' => "文件格式不对");
            } elseif($width != $height){//固定宽高图片
                if ($size[0] != $width) {
                    $response = array('error' => 3, 'msg' => "宽度只能是" . $width . "px");
                } elseif ($size[1] != $height) {
                    $response = array('error' => 4, 'msg' => "高度只能是" . $height . "px");
                } elseif ($filesize > $imgSize) {
                    $response = array('error' => 5, 'msg' => "图片不能大于" . $imgSize . "kb");
                }else{
                    try {
                        $existsstatus = $this->qiniu->exists($newFile);
                        if (!$existsstatus) {
                            //放入云存储
                            $this->qiniu->write($newFile, \Core\Common::getLocalAttachmentPathName($newFile));
                        }
                        $response = array('msg' => $this->imgDomain . $newFile, 'key' => $newFile, 'original' => $originalImgName);
                    } catch (\Exception $e) {
                        //
                    }
                }
            } elseif($width == $height){//正方形图片边长不大于800不小于固定值，做等比例缩放操作
                if ($size[0] != $size[1]) {
                    $response = array('error' => 6, 'msg' => "图片宽高必须相等");
                } elseif ($size[0] < $width || $size[0] > $maxWidth) {
                    $response = array('error' => 3, 'msg' => "宽度不能大于" . $maxWidth . "px，不能小于" . $width . "px");
                } elseif ($size[1] < $height || $size[1] > $maxHeight) {
                    $response = array('error' => 4, 'msg' => "高度不能大于" . $maxHeight . "px，不能小于" . $height . "px");
                } elseif ($filesize > $maxSize) {
                    $response = array('error' => 5, 'msg' => "图片不能大于" . $maxSize . "kb");
                }else{
                    try {

                        //等比例缩放图
                        $quality = \Core\Common::getQuality($path,$imgSize,0);
                        $thumb_name = \Core\Common::thumb($newFile, $width, $height, null, $quality);

                        //放入云存储
                        $existsstatus = $this->qiniu->exists($thumb_name);

                        if (!$existsstatus) {
                            //放入云存储
                            $this->qiniu->write($thumb_name, \Core\Common::getLocalAttachmentPathName($thumb_name));
                        }
                        $response = array('msg' => $this->imgDomain . $thumb_name, 'key' => $thumb_name, 'original' => $originalImgName);
                    } catch (\Exception $e) {
                        //
                    }
                }
            }
        }
        $this->sendHttpOutput($response);
    }

    /**
     * 图片预存储,商品详情页批量上传
     */
    public function doTempUploadAction() {

        //获取图片类型
        $type = $this->getRequest()->getQuery('type');

        //读取图片配置
        $typeWidth = $type."_width";
        $typeHeight = $type."_height";
        $typeSize = $type."_size";

        $width = $this->thumbConf->$typeWidth;
        $height = $this->thumbConf->$typeHeight;
        $imgSize = $this->thumbConf->$typeSize;

        //上传图片配置
        $maxWidth = $this->thumbConf->product_max_width;
        $maxHeight = $this->thumbConf->product_max_height;
        $maxSize = $this->thumbConf->product_max_size;
        if(empty($_FILES)){
            $response = array('error' => 1, 'msg' => "上传失败");
        } else {

            $tempFile = $_FILES['Filedata']['tmp_name'];

            // 验证图片格式
            $fileTypes = array('jpg', 'jpeg', 'JPG', 'JPEG');
            $fileParts = pathinfo($_FILES['Filedata']['name']);
            if (!in_array($fileParts['extension'], $fileTypes)) {
                $response = array('error' => 2, 'msg' => "文件格式不对");
            }else{
                //上传
                $keys = \Core\Common::uploadAttachmentReturnKeys();
                $newFile = $keys['Filedata'];

                $path = \Core\Common::getLocalAttachmentPathName($newFile);
                //原图放入云存储
                $originalImgName = "original_".$newFile;
                $originalExistsstatus = $this->qiniu->exists($originalImgName);
                if (!$originalExistsstatus) {
                    $this->qiniu->write($originalImgName, $path);
                }
                $size = getimagesize($path);
                $filesize = filesize($path) / 1024;

                if ($size[0] != $size[1]) {
                    $response = array('error' => 6, 'msg' => "宽高不相等");
                } elseif ($size[0] < $width || $size[0] > $maxWidth) {
                    $response = array('error' => 3, 'msg' => "宽度大于" . $maxWidth . "px或小于" . $width . "px");
                } elseif ($size[1] < $height || $size[1] > $maxHeight) {
                    $response = array('error' => 4, 'msg' => "高度大于" . $maxHeight . "px或小于" . $height . "px");
                } elseif ($filesize > $maxSize) {
                    $response = array('error' => 5, 'msg' => "大于" . $maxSize . "kb");
                } else {
                    try{
                        //468*468等比例缩放图
                        $quality = \Core\Common::getQuality($path,$imgSize,0);
                        $thumb_name = \Core\Common::thumb($newFile, $this->thumbConf->product_detail_width, $this->thumbConf->product_detail_height, null, $quality);

                        //放入云存储
                        $existsstatus = $this->qiniu->exists($thumb_name);
                        if (!$existsstatus) {
                            //放入云存储
                            $this->qiniu->write($thumb_name, \Core\Common::getLocalAttachmentPathName($thumb_name));
                        }

                        $response = array('msg' => $this->imgDomain . $thumb_name, 'key' => $thumb_name, 'original' => $originalImgName);
                    }catch(\Exception $e){

                    }
                }
            }
        }
        $this->sendHttpOutput($response);
    }

    /**
     * 排序生成列表页大小图
     */
    public function createListImgAction() {
        $pic = $this->getRequest()->getPost('img_path', 0);

        $list_small_img = $this->qiniu->imageThumb($pic, '0', $this->thumbConf->product_small_width, $this->thumbConf->product_small_height);
        $list_small_img = str_replace($this->imgDomain, '', $list_small_img);

        $list_big_img = $this->qiniu->imageThumb($pic, '0', $this->thumbConf->product_big_width, $this->thumbConf->product_big_height);
        $list_big_img = str_replace($this->imgDomain, '', $list_big_img);

        $cool_img = $this->qiniu->imageThumb($pic, '0', $this->thumbConf->cool_img_width, $this->thumbConf->cool_img_height);
        $cool_img = str_replace($this->imgDomain, '', $cool_img);

        $recommend_img = $this->qiniu->imageThumb($pic, '0', $this->thumbConf->recommend_img_width, $this->thumbConf->recommend_img_height);
        $recommend_img = str_replace($this->imgDomain, '', $recommend_img);

        $img = array('big' => $list_big_img, 'small' => $list_small_img,'cool' => $cool_img, 'recommend' => $recommend_img);

        $this->sendHttpOutput($img);
    }

}
