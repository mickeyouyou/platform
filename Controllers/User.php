<?php
use Core\BaseController;
use Core\Basic;
use Model\UserModel;
use Yaf\Dispatcher;

/**
 * @author fengzbao@qq.com
 * @copyright Copyright (c) fzb.me
 * @version $Id:1.0.0, User.php, 2015-08-01 15:04 created (updated)$
 */
class UserController extends BaseController
{
    private $userModel;

    public function init()
    {
        parent::init();
        $this->userModel = new UserModel();
    }

    /**
     * user list
     */
    public function indexAction()
    {
        $pageNo = $this->getRequest()->getQuery('page', 1);
        $where = array();
        $users = $this->userModel->get($where, $pageNo);

        $allNo = $this->userModel->count($where);

        $this->assign('users', $users);
        $this->assign('allno', $allNo);
        $this->assign('prepage', $pageNo - 1 == 0 ? 1 : $pageNo-1);
        $this->assign('nextpage', $pageNo + 1);
    }

    /**
     * search user_id
     */
    public function doSearchAction()
    {
        Dispatcher::getInstance()->disableView();
        $user_id = $this->getRequest()->getPost('user_id');
        $phone = $this->getRequest()->getPost('phone');
        $email = $this->getRequest()->getPost('email');

        $where  = array();

        if($user_id) {
            $where[] = 'uid = ' . $user_id;
        }

        if($phone) {
            $where[] = 'phone = "'.$phone.'"';
        }

        if($email) {
            $where[] = "email LIKE '%$email%'";
        }

        $user  = $this->userModel->get($where);

        $pageNo = $this->getRequest()->getQuery('page', 1);

        $allNo = $this->userModel->count($where);

        $this->assign('allno', $allNo);
        $this->assign('prepage', $pageNo - 1 == 0 ? 1 : $pageNo-1);
        $this->assign('nextpage', $pageNo + 1);

        $this->display('index', array('users' => $user));
    }


    public function addAction()
    {


    }

    /**
     * 添加用户操作
     */
    public function doAddAction()
    {
        $this->redirect('./index');
        Dispatcher::getInstance()->disableView();
        $this->log('error', var_export($this->getRequest()->getPost(), true));

        $user_name = $this->getRequest()->getPost('user_name');
        $phone     = $this->getRequest()->getPost('phone');
        $email     = $this->getRequest()->getPost('email');
        $password  = $this->getRequest()->getPost('password');

        if(empty($user_name) || empty($phone) || empty($email) || empty($password)) {
            echo ('关键字段为空');
        }

        // insert db
        $uid = $this->userModel->add(array(
            'username' => $user_name,
            'phone'     => $phone,
            'password'  => md5($password),
            'email'     => $email,
            'status'    => 1,
            'register_ip' => $this->getClientIp(),
            'create_time' => date('Y-m-d H:i:s'),
        ));

        if($uid) {
            $this->redirect('./index');
        }
     }

}