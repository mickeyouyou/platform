<?php

use Core\BaseController;
use Core\Basic;
use Core\Factory;
use Model\MessageModel;
use Yaf\Dispatcher;

class MessageController extends BaseController
{

    public $message;

    public function init()
    {
        parent::init();
        $this->message = new MessageModel();
    }


    public function indexAction()
    {
        Dispatcher::getInstance()->disableView();
        $page = $this->getRequest()->getQuery('page', 1);
        $where = array();
        $messages = $this->message->getList($where, $page);

        $allno = $this->message->count($where);

        $displayArray = array(
            'messages' => $messages,
            'allno'    => $allno,
            'prepage'  => $page - 1 == 0 ? 1 : $page-1,
            'nextpage' => $page + 1
        );
        $this->display('index', $displayArray);
    }


    public function searchAction()
    {

    }


    public function verifyAction()
    {
        \Yaf\Dispatcher::getInstance()->disableView();
        $id = $this->getRequest()->getQuery('id', 0);

        $where = array(
            'id'     => $id,
            'status' => 0,
            );
        $this->message->updateMessage($where, array('status' => 1));

        $this->redirect('./index');

    }


    public function deleteAction()
    {
        Dispatcher::getInstance()->disableView();
        $id = $this->getRequest()->getQuery('id', 0);

        $this->message->updateMessage(array('id' => $id), array('status' => -1));

        $this->redirect('./index');
    }


}



