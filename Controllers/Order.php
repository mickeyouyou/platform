<?php

use Core\BaseController;
use Core\Basic;
use Core\Common;
use Model\CourseModel;
use Model\OrderModel;
use Model\UserModel;
use Yaf\Dispatcher;
use Yaf\Exception;

class OrderController extends BaseController
{

    private $orderModel;
    public function init()
    {
        parent::init();
        $this->orderModel = new OrderModel();
    }

    /**
     * order list
     */
    public function indexAction()
    {

    }

    /**
     * search order list api
     * @throws \Yaf\Exception
     */
    public function doSearchAction()
    {
        Dispatcher::getInstance()->disableView();

        $postData = json_decode(file_get_contents('php://input'), true);

        $pageNo     = $postData['page'] ?:1;

        $where = array();
        if(isset($postData['order_id'])) {
            $where[] = 'order_id = ' . $postData['order_id'];
        }

        if(isset($postData['user_id'])) {
            $where[] = 'user_id = ' . $postData['user_id'];
        }

        if(isset($postData['start_time'])) {
            $where[] =  "pay_time >= '" . $postData['start_time'] . "'";
        }
        if(isset($postData['end_time'])) {
            $where[] = "pay_time < '" . $postData['end_time'] . "'";
        }
        if(isset($postData['pay_type'])) {
            $where[] = 'pay_type = ' . $postData['pay_type'];
        }
        if(isset($postData['total_fee'])) {
            $where[] = 'total_fee = ' . $postData['total_fee'];
        }
        if(isset($postData['status'])) {
            $where[] = 'status = ' . $postData['status'];
        }

        $allno = $this->orderModel->count($where);
        $order = $this->orderModel->get($where, $pageNo);

        $displayArray = array(
            'orders'   => $order,
            'allno'    => $allno,
            'prepage'  => $pageNo - 1 == 0 ? 1 : $pageNo-1,
            'nextpage' => $pageNo + 1
        );

        $this->sendHttpOutput($displayArray);
    }

    /**
     *
     * 生成订单
     */
    public function addAction()
    {

    }

    /**
     *
     * 现金支付方式订单生成
     * @return mixed
     */
    public function doAddAction()
    {
        Dispatcher::getInstance()->disableView();
        $data = $this->getRequest()->getPost();
        if(empty($data['uid']) || empty($data['cid'])) {
            $this->sendHttpOutput(array(
                'status' => false,
                'message' => 'use_id or course_id is empty'
            ));
        }

        $courseModel = new CourseModel();
        $course = $courseModel->getOne(array(
            'id' => $data['cid']
        ));

        if(empty($course)) {
            $this->sendHttpOutput(array(
                'status' => false,
                'message' => 'use_id or course_id is empty'
            ));
        }

        $order = array(
            'order_id'    => Common::generateOrderId(),
            'course_id'   => $data['cid'],
            'user_id'     => $data['uid'],
            'course_name' => $course['name'],
            'total_fee'   => $course['price'],
            'status'      => ORDER_FINISH_CODE,
            'pay_type'    => PAYTYPE_CASH,
            'trade_no'    => '',
            'extra_data'  => '',
            'create_time' => date('Y-m-d H:i:s'),
            'pay_time'    => date('Y-m-d H:i:s'), // 支付时间默认为创建时间
        );

        // insert db
        $order_id = $this->orderModel->add($order);

        if($order_id) {
            $return = array(
                'status' => true,
                'data'   => $order_id
            );
        } else {
            $return = array(
                'status' => false,
                'message' => 'order create failed',
                'data'   => '',
            );
        }

        $this->sendHttpOutput($return);
    }

    /**
     * comfirm the order
     */
    public function confirmAction()
    {
        Dispatcher::getInstance()->disableView();
        $uid = $this->getRequest()->getPost('uid');
        $cid = $this->getRequest()->getPost('cid');

        $userModel = new UserModel();
        $userInformation = $userModel->getOne(array(
            'uid' => $uid
        ));

        if(empty($userInformation)) {
            $return = array(
                'status' => false,
                'message' => '未能查找到用户信息'
            );
        }

        $courseModel = new CourseModel();
        $course = $courseModel->getOne(array(
            'id' => $cid
        ));

        if(empty($course)) {
            $this->sendHttpOutput($return  = array(
                'status' => false,
                'data' => '未能查找到课程信息'
            ));
        }

        $orders[] = array(
//            'order_id'    => Common::generateOrderId(),
            'course_id'   => $course['id'],
            'user_id'     => $uid,
            'course_name' => $course['name'],
            'total_fee'   => $course['price'],
            'status'      => ORDER_FINISH_CODE,
            'pay_type'    => PAYTYPE_CASH,
            'trade_no'    => '',
            'extra_data'  => '',
            'create_time' => date('Y-m-d H:i:s'),
            'pay_time'    => date('Y-m-d H:i:s'), // 支付时间默认为创建时间
        );
        $tpl = $this->_view->render('./order/table.phtml', array('orders' => $orders));

        $return = array(
            'status' => true,
            'data'   => $tpl
        );

        $this->sendHttpOutput($return);
    }

    /**
     * 订单关闭
     */
    public function closeAction()
    {
        Dispatcher::getInstance()->disableView();
        $order_id = $this->getRequest()->getQuery('order_id');

        if(empty($order_id)) {
            throw new Exception('订单号为空');
        }

        $updated = $this->orderModel->close($order_id);

        if($updated) {
            $this->redirect('./index');
        }

    }

    public function testAction()
    {
        $UID = str_pad('15', ORDER_AID_LENGTH, '0', STR_PAD_LEFT);

    }

}

