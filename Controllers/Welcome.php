<?php
use Core\BaseController;
use Core\Basic;
use Core\Factory;
use Yaf\Dispatcher;

/**
 * @author fengzbao@qq.com
 * @copyright Copyright (c) Beijing LuboTianDi Technology Co.,Ltd.
 * @version $Id:1.0.0, Welcome.php, 2015-08-02 20:26 created (updated)$
 */
class WelcomeController extends Basic
{
    private $session;

    public function init()
    {
        $this->session = Factory::session();
    }

    public function loginAction()
    {



    }


    public function doLoginAction()
    {
        // handle session
        Dispatcher::getInstance()->disableView();
        $admin_user = $this->getRequest()->getPost('username');
        $password   = $this->getRequest()->getPost('password');
        $verify     = $this->getRequest()->getPost('verify');

        if(md5(strtoupper($verify))  != $this->session->get('verify')) {
            header('Content-Type: text/html; charset=utf-8');
            echo "验证码错误";
            return false;
        }

        $adminModel = new \Model\AdminModel();
        $result = $adminModel->get(
            array(
                'admin_user' => $admin_user,
                'password'   => md5($password)
            )
        );

        if($result) {
            // session
            $this->session->set('admin_user', $admin_user);
            $this->redirect('../index/');
        } else {
            echo "密码错误或用户不存在";
        }
    }

    public function logoutAction()
    {
        Dispatcher::getInstance()->disableView();
        $this->session->del('admin_user');

        $this->redirect('./login');
    }


    public function verifyAction()
    {
        Dispatcher::getInstance()->disableView();
        Helper\Verify\Image::buildActiveImageVerify(4, 2);
    }

}