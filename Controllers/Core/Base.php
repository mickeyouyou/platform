<?php
namespace Core;

use Core\Basic;
use \Helper\Privilege\Operator;

/*
 *platform应用的基础controller
 */
abstract class BaseController extends Basic
{
    protected $operator;//操作人(本次访问页面的人)

    public function init()
    {
        parent::init();

        $this->checkAdminLogin();

        /*$ip = $this->getClientIp();
        if(!preg_match('/^192.168/', $ip))
        {
            if(!in_array($ip, array(
//                '106.39.8.138',
//                '106.39.79.250',
//                '42.121.120.122',
            )))
            {
                echo "请在办公网访问后台";exit;
            }
        }*/

    }

    public function checkAdminLogin()
    {
        $session = Factory::session();
        $admin_user = $session->get('admin_user');
        if(empty($admin_user)) {
            $this->redirect('../welcome/login');
        }

    }
}
