<?php
use Core\BaseController;
use Core\Basic;
use Model\AdminModel;
use Model\CityModel;
use Yaf\Dispatcher;

/**
 * @author fengzbao@qq.com
 * @copyright Copyright (c) Beijing LuboTianDi Technology Co.,Ltd.
 * @version $Id:1.0.0, Admin.php, 2015-08-02 17:53 created (updated)$
 */
class AdminController extends BaseController
{

    private $adminModel;
    public function init()
    {
        parent::init();
        $this->adminModel = new AdminModel();
    }

    /**
     *
     */
    public function indexAction()
    {
        $admins = $this->adminModel->get(array());
        $locationModel = new CityModel();
        $cities = $locationModel->getCities(array());

        $cityArray = array();
        foreach($cities as $n => $city) {
            $cityArray[$city['city_id']] = $city['city_name'];
        }

        $this->assign('admins', $admins);
        $this->assign('cityArray', $cityArray);
    }

    /**
     *
     */
    public function addAction()
    {
        $locationModel = new CityModel();
        $cities = $locationModel->getCities(array());

        $this->assign('cities', $cities);
    }

    /**
     *
     */
    public function doaddAction()
    {
        Dispatcher::getInstance()->disableView();
        $username = $this->getRequest()->getPost('username');
        $password = $this->getRequest()->getPost('password');
        $location = $this->getRequest()->getPost('location_id');

        $added =  $this->adminModel->add(
            array(
                'admin_user' => $username,
                'password' => md5($password),
                'create_time' => date('Y-m-d H:i:s'),
                'location_id' => $location,
                'isroot'      => '0',
                'status'      => '1'
            )
        );

        if($added) {
            $this->redirect('./index');
        }
    }

    /**
     *
     */
    public function deleteAction()
    {
        Dispatcher::getInstance()->disableView();
        $id = $this->getRequest()->getQuery('id', 0);

        $this->adminModel->delete(array('id' => $id));

        $this->redirect('./index');
    }

}