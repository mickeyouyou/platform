<?php
/**
 * 课程控制
 *
 *
 */
use Core\BaseController;
use Core\Basic;
use Model\ChapterModel;
use Model\CityModel;
use Model\ClassModel;
use Model\CourseModel;
use Model\CategoryModel;
use Yaf\Dispatcher;
use Yaf\Exception;

class CourseController extends BaseController
{
    private $cityModel;

    private $courseModel;

    private $chapterModel;

    private $classModel;
    private $categoryModel;

    public function init()
    {
        parent::init();
        $this->cityModel = new CityModel();
        $this->courseModel  = new CourseModel();
        $this->chapterModel = new ChapterModel();
        $this->classModel   = new ClassModel();
        $this->categoryModel = new CategoryModel();
    }

    /**
     * 课程列表
     */
    public function indexAction()
    {
        $city_id = $this->getRequest()->getQuery('city_id', 0);
        $cate_id = $this->getRequest()->getQuery('category_id', 0);

        if($city_id == 0 || $cate_id == 0) {
            $where = array(
            );
        } else {
            $where = array(
                'city_id' => $city_id,
                'cate_id' => $cate_id,
            );
        }

        $courses = $this->courseModel->get($where);


        $this->getView()->assign('courses', $courses);
        $this->getView()->assign('city_id', $city_id);
        $this->getView()->assign('cate_id', $cate_id);

    }

    /**
     * 为当前的业务城市增加课程
     */
    public function addAction()
    {
        $city_id = $this->getRequest()->getQuery('city_id', 0);
        $city = $this->cityModel->get(array(
            'city_id' => $city_id
        ));

        // read cate_ids by city_id
        $categories = $this->categoryModel->get(array(
            'city_id' => $city_id
        ));
        $this->assign('city_id', $this->getRequest()->getQuery('city_id'));
        $this->assign('city', $city);
        $this->assign('categories', $categories);
    }

    /**
     * add new course
     *
     */
    public function doAddAction()
    {
        Dispatcher::getInstance()->disableView();
        $imgs = \Core\Common::uploadAttachmentReturnKeys();

        $post = $this->getRequest()->getPost();
        $data = array(
            'city_id'      => $post['city_id'],
            'cate_id'      => $post['cate_id'],
            'name'         => $post['name'],
            'summary'      => $post['summary'],
            'description'  => $post['description'],
            'market_price' => $post['market_price'] * 100,
            'price'        => $post['price'] * 100,
            'status'       => 1,
            'teacher'      => $post['teacher'],
            'list_img'     => $imgs['list_img'],
            'detail_img'   => $imgs['detail_img'],
            'create_time'  => date('Y-m-d H:i:s')
        );

        $courseId = $this->courseModel->add($data);

        // insert chapters  and class
        $chapters = $post['chapter'];
        $chapterNum = count($chapters);
        if($chapterNum >1) {
            for($i=0; $i<$chapterNum; $i++) {
                // insert database
                $chapter = array(
                    'course_id'   => $courseId,
                    'name'        => $chapters[$i],
                    'create_time' => date('Y-m-d H:i:s')
                );
                $chapterId = $this->chapterModel->add($chapter);

                $className      = $this->getRequest()->getPost('chapter_' . $i . '_class_name');
                $classUv        = $this->getRequest()->getPost('chapter_' . $i . '_uv');
                $classWare      = $this->getRequest()->getPost('chapter_' . $i . '_classware');
                $classAudition  = $this->getRequest()->getPost('chapter_' . $i . '_is_audition');
                $classNum       = count($className);
                for($j=0; $j< $classNum; $j++) {
                    $classContent = array(
                        'name'        => $className[$j],
                        'uv'          => $classUv[$j],
                        'classware'   => $classWare[$j],
                        'is_audition' => $classAudition[$j],
                        'chapter_id'  => $chapterId,
                        'course_id'   => $courseId,
                        'create_time' => date('Y-m-d H:i:s')
                    );

                    $inserted =$this->classModel->add($classContent);
                }
            }
        }



        if($courseId) {
            $this->redirect('./index');
        }
    }

    /**
     * edit a course
     *
     */
    public function editAction()
    {
        $cid      = $this->getRequest()->getQuery('cid');
        $course   = $this->courseModel->get(array('id' => $cid));
        $chapters = $this->chapterModel->get(array('course_id' => $cid));
        $classes  = $this->classModel->get(array('course_id' => $cid));

        $city    = $this->cityModel->get(array(
            'city_id' => $course[0]['city_id']
        ));
        $categories = $this->categoryModel->get(array(
            'city_id' => $course[0]['city_id']
        ));

        $chapterClass = array();
        foreach($classes as $n => $class) {
            foreach($chapters as $k => $chapter) {
                if($class['chapter_id'] == $chapter['id']) {
                    $chapterClass[$chapter['id']]['name']     = $chapter['name'];
                    $chapterClass[$chapter['id']]['class'][]  = $class;
                }
            }
        }

        $this->assign('course',     $course[0]);
        $this->assign('city',     $city);
        $this->assign('categories', $categories);
        $this->assign('chapterClass', $chapterClass);
    }

    /**
     * update course data
     * $post
     */
    public function doEditAction()
    {
        Dispatcher::getInstance()->disableView();
        $courseId = $this->getRequest()->getPost('course_id');
        // chapters
        $chapters = $this->getRequest()->getPost('chapter');

        if(!empty($_FILES['detail_img']['name']) || !empty($_FILES['list_img']['name'])) {
            // images
            $imgs = \Core\Common::uploadAttachmentReturnKeys();
        }

        /**********************************************
         *
         * course data handle
         *
         *********************************************/
        $post = $this->getRequest()->getPost();
        $data = array(
            'city_id'      => $post['city_id'],
            'cate_id'      => $post['cate_id'],
            'name'         => $post['name'],
            'summary'      => $post['summary'],
            'description'  => $post['description'],
            'market_price' => $post['market_price'] * 100,
            'price'        => $post['price'] * 100,
            'teacher'      => $post['teacher'],
//            'status'       => 1,
            'update_time'  => date('Y-m-d H:i:s')
        );

        if(!empty($imgs)) {
            if(!empty($imgs['list_img']) && isset($imgs['list_img'])) {
                $data['list_img'] = $imgs['list_img'];
            }

            if(!empty($imgs['detail_img']) && isset($imgs['list_img'])) {
                $data['detail_img'] = $imgs['detail_img'];
            }
        }

        $this->courseModel->update($data, array('id' => $courseId));

        /**********************************************************************
         *
         * course data handle
         *
         *********************************************************************/
        $chapterNum = count($chapters);
        if($chapterNum) {
            // delete the chapter data from database where course_id = $courseId
            $this->chapterModel->delete($courseId);
            // delete first
            $this->classModel->delete($courseId);
            $allClassNum = 1;
            for($i=0; $i<$chapterNum; $i++) {
                // insert database
                $chapter = array(
                    'course_id'   => $courseId,
                    'name'        => $chapters[$i],
                    'create_time' => date('Y-m-d H:i:s')
                );
                $chapterId = $this->chapterModel->add($chapter);

                $className      = $this->getRequest()->getPost('chapter_' . $i . '_class_name');
                $classUv        = $this->getRequest()->getPost('chapter_' . $i . '_uv');
                $classWare      = $this->getRequest()->getPost('chapter_' . $i . '_classware');
                $classAudition  = $this->getRequest()->getPost('chapter_' . $i . '_is_audition');
                $classNum       = count($className);
                for($j=0; $j< $classNum; $j++) {
                    $classContent = array(
                        'name'        => $className[$j],
                        'uv'          => $classUv[$j],
                        'classware'   => $classWare[$j],
                        'is_audition' => $classAudition[$j],
                        'chapter_id'  => $chapterId,
                        'course_id'   => $courseId,
                        'create_time' => date('Y-m-d H:i:s')
                    );

                    $return =$this->classModel->add($classContent);
                    $allClassNum++;
                }
                unset($chapterId);
            }

            // update all class num
            $this->courseModel->update(array('classes_num' => $allClassNum), array('id' => $courseId));
        }
        if($return) {
            $this->redirect('./index');
        }
    }

    /**
     * delete all info of course
     * @throws \Yaf\Exception
     * @internal param $cid
     */
    public function deleteAction()
    {
        Dispatcher::getInstance()->disableView();
        $cid = $this->getRequest()->getQuery('cid');
        try{
            // delete course
            $n = $this->courseModel->delete($cid);

            // delete chapter
            $this->chapterModel->delete($cid);

            // delete class
            $this->classModel->delete($cid);

        } catch (Exception $e) {
            throw new Exception('删除课程失败');
        }

        if($n) {
            $this->redirect('./index');
        }
    }

    /**
     * 课程下线,下线状态为-1,取所有课程时取非-1的情况
     * @throws \Yaf\Exception
     */
    public function offlineAction()
    {
        Dispatcher::getInstance()->disableView();
        $cid = $this->getRequest()->getQuery('cid');
        try{
            // offline course
            $isExist = $this->courseModel->get(array('id' => $cid));
            if(!$isExist){
                throw new \Exception('课程不存在');
            }
            $n = $this->courseModel->update(array('status' => -1), array('id' => $cid));

        } catch (Exception $e) {
            throw new Exception('下线课程操作失败');
        }

        if($n) {
            $this->redirect('./index');
        }

    }

    /**
     * 更新课程
     * @throws \Exception
     * @throws \Yaf\Exception
     */
    public function onlineAction()
    {

        Dispatcher::getInstance()->disableView();
        $cid = $this->getRequest()->getQuery('cid');
        try{
            // offline course
            $isExist = $this->courseModel->get(array('id' => $cid));
            if(!$isExist){
                throw new \Exception('课程不存在');
            }
            $n = $this->courseModel->update(array('status' => 1), array('id' => $cid));

        } catch (Exception $e) {
            throw new Exception('上线课程操作失败');
        }

        if($n) {
            $this->redirect('./index');
        }
    }

}
