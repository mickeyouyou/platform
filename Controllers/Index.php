<?php

use Core\BaseController;
use Core\Basic;
use Core\Factory;
use Model\CityModel;
use Model\CourseModel;
use Model\OrderModel;
use Model\UserModel;

class IndexController extends BaseController
{
    public function init()
    {
        parent::init();

    }

    public function indexAction()
    {

        $cities = new CityModel();
        $cities = $cities->count(array());

        $order = new OrderModel();

        $userModel = new UserModel();
        $userNum = $userModel->count();

        $courseModel = new CourseModel();
        $courseNum = $courseModel->count();

        $this->assign('cityNum', $cities);
        $this->assign('courseNum', $courseNum);
        $this->assign('userNum', $userNum);
        $this->assign('orderNum', $order->count());

    }


}



