<?php

use Core\BaseController;
use Core\Basic;
use Model\CategoryModel;
use Yaf\Dispatcher;
use Yaf\Exception;

class CategoryController extends BaseController
{
    /**
     *
     *
     * @var string 业务城市id
     */
    private $city_id;

    private $categoryModel;

    public function init()
    {
        parent::init();
        $this->categoryModel = new CategoryModel();

    }

    public function indexAction()
    {
        $city_id = $this->getRequest()->getQuery('city_id');
        $categories = $this->categoryModel->get(array('city_id' => $city_id));

        $this->getView()->assign('city_id', $city_id);
        $this->getView()->assign('categories', $categories);
    }

    public function addAction()
    {
        $city_id = $this->getRequest()->getQuery('city_id');

        $this->assign('city_id', $city_id);
    }


    public function doAddAction()
    {
        Dispatcher::getInstance()->disableView();
        $category = $this->getRequest()->getPost('category');
        $type     = $this->getRequest()->getPost('type');
        $city_id  = $this->getRequest()->getPost('city_id');
        $page_url = $this->getRequest()->getPost('page_url');


        if(empty($category)) {
            throw new Exception('内容为空');
        }

        $return = $this->categoryModel->add(array(
            'name'    => $category,
            'type'    => $type,
            'city_id' => $city_id,
            'page_url' => $page_url,
        ));
        var_dump($return);
        if($return) {
            $this->redirect('./index?city_id=' . $city_id);
        }
    }


    public function editAction()
    {
        $categoryId = $this->getRequest()->getQuery('cate_id');

        if(empty($categoryId)) {
            throw new Exception('课程分类ID 为空');
        }

        $category = $this->categoryModel->getOne(array('cate_id' => $categoryId));

        $this->getView()->assign('category', $category);
    }


    public function doeditAction()
    {
        Dispatcher::getInstance()->disableView();
        $data = array(
            'cate_id'     => $this->getRequest()->getPost('cate_id'),
            'name'     => $this->getRequest()->getPost('category'),
            'page_url' => $this->getRequest()->getPost('page_url')
        );

        $city_id = $this->getRequest()->getPost('city_id');
        $type    = $this->getRequest()->getPost('type');

        foreach($data as $name => $value) {
            if(empty($data[$name])) {
                throw new Exception($name . '字段内容为空');
            }
        }

        $return = $this->categoryModel->update(array(
            'name'     => $data['name'],
            'type'     => $type,
            'page_url' => $data['page_url'],
        ), array('cate_id' => $data['cate_id']));

        if($return) {
            $this->redirect('./index?city_id=' . $city_id);
        }
    }


    public function deleteAction()
    {
        Dispatcher::getInstance()->disableView();
        $id      = $this->getRequest()->getQuery('cate_id', 0);
        $city_id = $this->getRequest()->getQuery('city_id', 0);

        $this->categoryModel->delete(array('cate_id' => $id));

        $this->redirect('./index?city_id=' . $city_id);
    }
}