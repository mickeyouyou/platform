<?php
use Core\BaseController;
use Core\Basic;
use Model\BannerModel;
use Yaf\Dispatcher;

/**
 * 
 * Banner 管理
 * @author fengzbao@qq.com 
 * @copyright Copyright (c) Beijing LuboTianDi Technology Co.,Ltd.
 * @version $Id:1.0.0, Banner.php, 2015-07-07 10:04 created (updated)$
 */

class BannerController extends BaseController
{
    private $bannerModel;

    public function init()
    {
        parent::init();
        $this->bannerModel = new BannerModel();

    }

    public function indexAction()
    {
        $banners = $this->bannerModel->get(array());

        $this->assign('banners', $banners);
    }

    public function addAction()
    {



    }


    public function doAddAction()
    {
        Dispatcher::getInstance()->disableView();

        $post = $this->getRequest()->getPost();


        if(!empty($_FILES['banner_img']['name'])) {
            $img = \Core\Common::uploadAttachmentReturnKeys();
        }

        $data = array(
            'introduction' => $post['intro'],
            'href'        => $post['href'],
            'priority'    => $post['priority'],
            'img'         => $img['banner_img'],
            'create_time' => date('Y-m-d H:i:s')
        );

        $return = $this->bannerModel->add($data);

        if($return) {
            $this->redirect('./index');
        }
    }

    public function editAction()
    {
        $id = $this->getRequest()->getQuery('id', 0);

        $banner = $this->bannerModel->get(array('id' => $id));

        $this->assign('banner', $banner[0]);
    }

    public function doeditAction()
    {
        $id = $this->getRequest()->getPost('id', 0);

        $data = array(
            'introduction' => $this->getRequest()->getPost('intro'),
            'href'         => $this->getRequest()->getPost('href'),
            'priority'     => $this->getRequest()->getPost('priority'),
        );

        if(!empty($_FILES['banner_img']['name'])) {
            $img = \Core\Common::uploadAttachmentReturnKeys();
            $data['img'] = $img['banner_img'];
        }

        $return = $this->bannerModel->update($data, array('id' => $id));

        if($return) {
            $this->redirect('./index');
        }
    }

    /**
     *
     * delete banner
     */
    public function deleteAction()
    {
        Dispatcher::getInstance()->disableView();
        $id = $this->getRequest()->getQuery('id', 0);

        $this->bannerModel->delete(array('id' => $id));

        $this->redirect('./index');
    }

}